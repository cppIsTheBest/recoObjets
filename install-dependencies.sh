#!/bin/bash

# Install the necessary packages
sudo apt-get update
sudo apt-get install intltool libgstreamer-plugins-base1.0-dev libusb-1.0-0-dev libnotify-dev libgtk-3-dev libaudit-dev libcap-ng-dev python3-pyqt5 python3-termcolor libatlas-base-dev

# Needed python dependencies
sudo pip3 install opencv-python

# Install aravis, for the GigE camera
wget http://ftp.acc.umu.se/pub/GNOME/sources/aravis/0.5/aravis-0.5.11.tar.xz
tar -xf aravis-0.5.11.tar.xz
cd aravis-0.5.11
./configure
make
sudo make install
cd ..
rm aravis-0.5.11.tar.xz
sudo mv /usr/local/lib/libaravis-0.6.* /usr/lib/
