#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <dirent.h>
#include <string.h>
#include "hog.h"



/***************************************************/
/* CHECK DOCUMENTATION IN THE MATCHING HEADER FILE */
/***************************************************/



void computeAndWriteHOGsToFile(const char* dirPath,
                                    const char* outputPath, int target,
                                    const char* writeMode) {
    struct dirent *de;
    DIR *dir = opendir(dirPath);

    if (dir == NULL) {
        printf("ERROR : Could not open directory %s", dirPath);
        return;
    }

    // creates output file
    FILE* outFile = fopen(outputPath, writeMode);

    // compute all HOG descriptors, for each image in the directory
    while ((de = readdir(dir)) != NULL) {
        // don't do anything for entries "." and ".." in directory
        if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0)
            continue;

        // get full path of the image
        char *full_path = concatenate(dirPath, de->d_name);

        // compute HOG descriptors for the current image
        printf("Computing HOG descriptors for image %s\n", full_path);
        HOGDescriptors descriptors = computeHOGDescriptors(full_path);

        // write the target value
        switch(target) {
            case 0:
                fprintf(outFile, "0");
                break;
            case 1:
                fprintf(outFile, "+1");
                break;
            case -1:
                fprintf(outFile, "-1");
                break;
            default:
                printf("ERROR : target value must be 0, 1 or -1\n");
                break;
        }

        // write the HOG descriptors to the output file
        long int featureID = 1;
        for (int bloc=0; bloc<descriptors.nbBlocs; bloc++) {
            for (int cell=0; cell<descriptors.normalizedHOGs[bloc].nbCells; cell++) {
                for (int bin=0; bin<NB_BIN_PER_HISTOGRAM; bin++) {
                    double featureValue = descriptors.normalizedHOGs[bloc].hogs[cell][bin];
                    fprintf(outFile, " %ld:%f", featureID, featureValue);
                    featureID ++;
                }
            }
        }
        // write the name of the file as a commentary at the end of the line
        fprintf(outFile, " #%s\n", de->d_name);

        free(full_path);
        freeMemoryHOGDescriptors(&descriptors);
    }
    fclose(outFile);
}



HOGDescriptors computeHOGDescriptors(const char* imgPath) {
    // load image
    PGMData* img = malloc(sizeof(PGMData));
    readPGM(imgPath, img);

    // allocate space for gradient, histograms and final descriptors
    IMGGradient grad = createIMGGradient(img->col, img->row);
    Histograms hogs = createHistograms(img->col, img->row);
    HOGDescriptors descriptors = createHOGDescriptors(img->col, img->row);

    // compute hogs
    gradientComputation(&grad, img);
    histogramsConstruction(grad, &hogs);
    blocsConstruction(hogs, &descriptors);

    // free memory allocated for gradient, histograms and image
    freeMemoryIMGGradient(&grad);
    freeMemoryHistograms(&hogs);
    deallocate_dynamic_matrix(img->matrix, img->row);

    return descriptors;
}



void createGradientImage(const char* imgPath, const char* gradientImgPath) {
    // load image
    PGMData* img = malloc(sizeof(PGMData));
    readPGM(imgPath, img);

    // allocate space for gradient
    IMGGradient grad = createIMGGradient(img->col, img->row);

    // compute gradient
    gradientComputation(&grad, img);

    // replace the pixels values by the gradient values
    for (int i=0; i<grad.height; i++) {
        for (int j=0; j<grad.width; j++) {
            img->matrix[i][j] = grad.module[i][j];
        }
    }

    // creates the new image
    writePGM(gradientImgPath, img);

    // free memory allocated for gradient and image
    freeMemoryIMGGradient(&grad);
    deallocate_dynamic_matrix(img->matrix, img->row);
}



IMGGradient createIMGGradient(int width, int height) {
    if (width > 0 && height > 0) {
        IMGGradient grad;

        grad.angle = (double**)malloc(height * sizeof(double*));
        grad.module = (double**)malloc(height * sizeof(double*));
        for (int i=0; i<height; i++) {
            grad.angle[i] = (double*)malloc(width * sizeof(double));
            grad.module[i] = (double*)malloc(width * sizeof(double));
        }
        grad.width = width;
        grad.height = height;

        return grad;
    }
    else {
        printf("ERROR : Cannot allocate memory for the gradient.");
        printf(" The dimensions must be greater than 0.\n");
        return (IMGGradient){NULL, NULL, 0, 0};
    }
}



Histograms createHistograms(int width, int height) {
    if (width > 0 && height > 0
                        && width % CELL_SIZE == 0 && height % CELL_SIZE == 0) {
        Histograms hogs;

        hogs.nbCells = (width / CELL_SIZE) * (height / CELL_SIZE);
        hogs.nb_cellX = width / CELL_SIZE;
        hogs.nb_cellY = height / CELL_SIZE;
        hogs.hogs = (double**)malloc(hogs.nbCells * sizeof(double*));
        for (int k=0; k<hogs.nbCells; k++)
            hogs.hogs[k] = (double*)malloc(NB_BIN_PER_HISTOGRAM * sizeof(double));
    
        return hogs;
    }
    else {
        printf("%d %d\n", width, height);
        printf("ERROR : Cannot allocate memory for the histograms.");
        printf(" The dimensions must be > 0 and a multiple of CELL_SIZE.\n");
        return (Histograms){NULL, 0, 0, 0};
    }
}



HOGDescriptors createHOGDescriptors(int width, int height) {
    if (width > 0 && height > 0
               && width % CELL_SIZE == 0 && height % CELL_SIZE == 0
               && width >= CELL_SIZE*BLOC_SIZE && height >= CELL_SIZE*BLOC_SIZE) {

        HOGDescriptors descriptors;

        // compute number of blocs (full overlapping, let's see if it works)
        int offset = BLOC_SIZE - 1;
        descriptors.nbBlocs = (width/CELL_SIZE - offset) * (height/CELL_SIZE - offset);
        descriptors.nb_blocX = width/CELL_SIZE - offset;
        descriptors.nb_blocY = height/CELL_SIZE - offset;
        descriptors.normalizedHOGs =
                  (Histograms*)malloc(descriptors.nbBlocs * sizeof(Histograms));

        // for each bloc, allocate memory for the histograms inside it
        int pixPerBloc = BLOC_SIZE * CELL_SIZE;
        for (int k=0; k<descriptors.nbBlocs; k++) {
            descriptors.normalizedHOGs[k] =
                                    createHistograms(pixPerBloc, pixPerBloc);
        }

        return descriptors;
    }
    else {
        printf("ERROR : Cannot allocate memory for the HOG Descriptors.");
        printf(" The dimensions must be > 0, a multiple of CELL_SIZE,");
        printf(" and greater than the size of a bloc of cells.\n");
        return (HOGDescriptors){NULL, 0, 0, 0};
    }
}



void freeMemoryIMGGradient(IMGGradient* grad) {
    if (grad == NULL || grad->angle != NULL) {
        for (int i=0; i<grad->height; i++) {
            free(grad->angle[i]);
            free(grad->module[i]);
        }
        free(grad->angle);
        free(grad->module);
        *grad = (IMGGradient){NULL, NULL, 0, 0};
    }
    else {
        printf("ERROR : cannot free memory for IMGGradient object because");
        printf(" there is no memory allocated for it.\n");
    }
}



void freeMemoryHistograms(Histograms* hogs) {
    if (hogs == NULL || hogs->hogs != NULL) {
        for (int k=0; k<hogs->nbCells; k++)
            free(hogs->hogs[k]);
        free(hogs->hogs);
        *hogs = (Histograms){NULL, 0, 0, 0};
    }
    else {
        printf("ERROR : cannot free memory for Histograms object because");
        printf(" there is no memory allocated for it.\n");
    }
}



void freeMemoryHOGDescriptors(HOGDescriptors* descriptors) {
    if (descriptors == NULL || descriptors->normalizedHOGs != NULL) {
        for (int k=0; k<descriptors->nbBlocs; k++)
            freeMemoryHistograms(&descriptors->normalizedHOGs[k]);
        free(descriptors->normalizedHOGs);
        *descriptors = (HOGDescriptors){NULL, 0, 0, 0};
    }
    else {
        printf("ERROR : cannot free memory for HOGDescriptors object because");
        printf(" there is no memory allocated for it.\n");
    }
}



void gradientComputation(IMGGradient* grad, PGMData* img) {
    double grad_x, grad_y;

    /* compute gradient angle for each pixel using the following masks :
       horizontal (-1; 0; 1)
       vertical (-1; 0; 1)T
    */
    for (int i=0; i<grad->height; i++) {
        for (int j=0; j<grad->width; j++) {
            // compute gradient value for this pixel on the y-axis
            grad_y = 0;
            if (i != 0)
                grad_y += (double)(-1 * img->matrix[i-1][j]);
            if (i != grad->height-1)
                grad_y += (double)(1 * img->matrix[i+1][j]);

            // compute gradient value for this pixel on the x-axis
            grad_x = 0;
            if (j != 0)
                grad_x += (double)(-1 * img->matrix[i][j-1]);
            if (j != grad->height-1)
                grad_x += (double)(1 * img->matrix[i][j+1]);

            /* compute angle */
            grad->module[i][j] = sqrt(pow(grad_x,2) + pow(grad_y,2));
            if (grad_x == 0)
                grad->angle[i][j] = PI / 2;
            else
                grad->angle[i][j] = atan( grad_y / grad_x );

            /* we don't care about the direction, only the orientation, and we
               want a result between 0 and PI */
            if (grad->angle[i][j] < 0)
                grad->angle[i][j] += PI;
        }
    }
}



void histogramsConstruction(IMGGradient grad, Histograms* hogs) {
    /* Fill histograms with zeroes */
    for (int k=0; k<hogs->nbCells; k++)
        for (int l=0; l<NB_BIN_PER_HISTOGRAM; l++)
            hogs->hogs[k][l] = 0.0;

    /* Construct histograms. Pixels votes are weighted by the module of
       their gradient. */
    for (int i=0; i<grad.height; i++) {
        for (int j=0; j<grad.width; j++) {
            // compute the index of the HOG corresponding to this pixel
            int HOGrow = i / CELL_SIZE;
            int HOGcolumn = j / CELL_SIZE;
            int HOGindex = HOGrow * (grad.width / CELL_SIZE) + HOGcolumn;

            // selecting the right bin (between 0 and NB_BIN_PER_HISTOGRAM-1)
            int bin = (int)round(grad.angle[i][j] / PI * (NB_BIN_PER_HISTOGRAM-1));

            /* adding the weight of the pixel's gradient to the selected
               histogram's bin */
            hogs->hogs[HOGindex][bin] += grad.module[i][j];
        }
    }
}



void blocsConstruction(Histograms hist, HOGDescriptors* descriptors) {
    /* for each possible bloc of BLOC_SIZE * BLOC_SIZE cells
       (with overlapping), compute and store the normalized histograms
    */
    for (int blocX=0; blocX<descriptors->nb_blocX; blocX++) {
        for (int blocY=0; blocY<descriptors->nb_blocY; blocY++) {
            // compute the mean of histograms for this bloc
            double hist_mean = 0;
            for (int k=0; k<BLOC_SIZE; k++) {
                for (int l=0; l<BLOC_SIZE; l++) {
                    for (int bin=0; bin<NB_BIN_PER_HISTOGRAM; bin++) {
                        int histIndex = blocY*hist.nb_cellX + blocX;
                        hist_mean += hist.hogs[histIndex][bin];
                    }
                }
            }
            // store the histograms normalized using the L1-sqrt norm
            for (int k=0; k<BLOC_SIZE; k++) {
                for (int l=0; l<BLOC_SIZE; l++) {
                    for (int bin=0; bin<NB_BIN_PER_HISTOGRAM; bin++) {
                        int histIndex = (blocY+l)*hist.nb_cellX + blocX + k;
                        int histIndexInBloc = l*BLOC_SIZE + k;
                        int blocIndex = blocY * descriptors->nb_blocX + blocX;
            descriptors->normalizedHOGs[blocIndex].hogs[histIndexInBloc][bin] =
                         sqrt(hist.hogs[histIndex][bin] / (hist_mean + EPSILON));
                    }
                }
            }
        }
    }
}



char* concatenate(const char* str1, const char* str2) {
    int l1 = strlen(str1);
    int l2 = strlen(str2);
    char *result = (char*)malloc(sizeof(char)*(l1 + l2 + 1));
    strcpy(result, str1);
    strcat(result, str2);

    return result;
}

