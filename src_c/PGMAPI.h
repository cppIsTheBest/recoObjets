//
// Created by matbl on 09/02/2018.
//

#ifndef PROJETLONG_PGMAPI_H
#define PROJETLONG_PGMAPI_H

#include <stdio.h>

/**
 * Structure of a PGM file, which consists of :
 *     - an integer for the number of rows in the file
 *     - an integer for the number of columns in the file
 *     - an integer for the highest value for a pixel
 *     - a matrix of integers that represents the raw pixels
 */
typedef struct PGMData {
    int row;
    int col;
    int max_gray;
    int **matrix;
} PGMData;

/**
 * Allocates the matrix for a PGM file dynamically
 * @param row the number of rows in the matrix
 * @param col the number of columns in the matrix
 * @return a pointer on the allocated matrix
 */
int **allocate_dynamic_matrix(int row, int col);

/**
 * Deallocates the matrx for a PGM file dynamically
 * @param matrix the pointer on the matrix of the PGM file
 * @param row the number of rows in the matrix to deallocate properly
 */
void deallocate_dynamic_matrix(int **matrix, int row);

/**
 * Skips the comments in a PGM file (used for better parsing)
 * @param fp the PGM file that need to be parsed without comments
 */
void SkipComments(FILE *fp);

/**
 * Reads a PGM file and stores all the information in a PGMData variable with the appropriate structure
 * @param file_name the name of the PGM file that will be read
 * @param data the PGMData that will be completed with the information from the file
 */
void readPGM(const char *file_name, PGMData *data);

/**
 * Writes all the information of a PGMData variable into a PGM file
 * @param filename the file to be created and written into
 * @param data the data to write into the file
 */
void writePGM(const char *filename, const PGMData *data);

/**
 * Debugging method
 * @param data the data to be printed for debug
 */
void print(PGMData *data);

#endif //PROJETLONG_PGMAPI_H