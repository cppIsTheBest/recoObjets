#include <arv.h>
#include <stdlib.h>
#include <signal.h>
#include <stdio.h>
#include <stdint.h>
#include <sys/time.h>
#include <unistd.h>
#include <fcntl.h>

struct timeval now;

/* Header for PGM files (P5 640 480 255) */
uint8_t header_buffer[15] = {0x50, 0x35, 0x0a, 0x36, 0x34, 0x30, 0x20, 0x34, 0x38, 0x30,
                             0x0a, 0x32, 0x35, 0x35, 0x0a};
int header_size = 15;

/* Used to store the directory in which to store the files */
char *saved_file_name;
/* Used to store the complete name of the files */
char saved_file[256];

size_t image_size;

GMainLoop *loop;

/* Used to check if the capture should be stopped or not (when sending SIGINT for example) */
gboolean running = TRUE;


/* Called with SIGINT, sets the running boolean to false */
void stop_loop(int signal) {
  if (signal == SIGINT)
    running = FALSE;
}


/* Called when the connection to the camera is lost, sets the running boolean to false */
void lost(ArvGvDevice *gv_device) {
  running = FALSE;
}


/* Called periodically, stops the capture if running is set to false */
gboolean check_run() {
  if (running == FALSE) {
    g_main_loop_quit(loop);
  }
}


/* Called when a new image is available.
   The received image uses a resolution of 1280x960, so we reduce it to 640x480 */
void do_capture(ArvStream *stream) {
    ArvBuffer *buffer = arv_stream_try_pop_buffer(stream);
    if (buffer != NULL) {
        /* Image processing here */
        const void *data = arv_buffer_get_data(buffer, &image_size);
        unsigned char *buffer_data = (unsigned char*) data;

        /* Here we get the current time to compute the timestamp (filename) */
        struct timeval tv = {0};
        tv.tv_sec = 2;
        gettimeofday(&now, NULL);

        /* We store the filename in saved_file */
        sprintf(saved_file, "%s/%ld.pgm", saved_file_name, (now.tv_sec * 1000 + now.tv_usec/1000));

        /* Creation of the image file */
        int saved_fd = open(saved_file, O_CREAT | O_WRONLY, 0666);

        /* Writing of the header */
        write(saved_fd, header_buffer, header_size);

        /* Here we reduce the size of the image (to 640x480) */
        unsigned char new_buffer_data[480][640];

        int i, j;
        for (i = 0; i < 480; i++) {
          for (j = 0; j < 640; j++) {
            new_buffer_data[i][j] = buffer_data[i*2560 + j*2];
          }
        }
 
        /* Writing of the image data */
        write(saved_fd, new_buffer_data, image_size/4);
        close(saved_fd);

        arv_stream_push_buffer(stream, buffer);
    }
}


int main (int argc, char **argv) {
    if (argc != 2) {
      printf("Usage : %s DIR\n\nThe images will be saved in DIR (must exist!)\n", argv[0]);
      return 1;
    }
    saved_file_name = argv[1];

    ArvCamera *camera;
    int i;

    /* Mandatory glib type system initialization */
    arv_g_type_init();

    /* Instantiation of the first available camera */
    camera = arv_camera_new(NULL);

    if (camera != NULL) {
        /* Set region to a 1280x960 pixel area (no crop)  */
        arv_camera_set_region(camera, 0, 0, 1280, 960);
        /* Set frame rate to 10 fps */
        arv_camera_set_frame_rate(camera, 10.0);
        /* Set exposure to an acceptable value for indoor captures */
        arv_camera_set_exposure_time(camera, 20000);
        /* Set pixel format to 8bit raw data */
        arv_camera_set_pixel_format(camera, 0x01080001);

        /* Get the number of bytes per image */
        image_size = (size_t) arv_camera_get_payload(camera);

        /* Creation of a camera stream */
        ArvStream* stream = arv_camera_create_stream(camera, NULL, NULL);

        if (stream != NULL) {
          /* Create buffers for the stream */
          for (i = 0; i < 10; i++)
            arv_stream_push_buffer(stream, arv_buffer_new(image_size, NULL));
          
          /* Start the camera stream */
          arv_camera_start_acquisition(camera);

          /* Set callback for the "new-buffer" event and enable signals emission */
          g_signal_connect(stream, "new-buffer", G_CALLBACK(do_capture), NULL);
          arv_stream_set_emit_signals(stream, TRUE);

          /* Set callback to stop the stream when stopping the program or losing the camera */
          g_timeout_add_seconds (1, check_run, NULL);
          g_signal_connect (arv_camera_get_device(camera), "control-lost", G_CALLBACK(lost), NULL);

          /* Set handler for SIGINT signal */
          signal(SIGINT, stop_loop);

          /* Run the main loop */
          loop = g_main_loop_new(NULL, FALSE);
          g_main_loop_run(loop);

          /* Unref the stream */
          arv_stream_set_emit_signals(stream, FALSE);
          g_object_unref(stream);
        } else
          printf("No stream created\n");
 
        /* Unref the camera */
        g_object_unref(camera);
    } else
        printf("No camera found\n");

    return 0;
}
