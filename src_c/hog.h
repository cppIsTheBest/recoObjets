#ifndef HOG_H_INCLUDED
#define HOG_H_INCLUDED

#include "PGMAPI.h"


/****************************************************************************/
/* This file defines functions that can be used to compute HOGs of an image */
/****************************************************************************/



/* Constants */
#define NB_BIN_PER_HISTOGRAM 9
#define CELL_SIZE 8
#define BLOC_SIZE 3
#define PI 3.14159265358979323846
#define EPSILON 0.0000001



/* Structure storing the angular value and the module of the gradient for each
   pixel in an image. */
struct IMGGradient_s {
    double** angle;
    double** module;
    int width;
    int height;
};
typedef struct IMGGradient_s IMGGradient;


/* Structure storing the Histograms of an image. */
struct Histograms_s {
    /* Array of histograms, one per cell (a cell is a square matrix of pixels).
       A histogram is an array of size NB_BIN_PER_HISTOGRAM. It associates to
       each bin the combined weights of the matching gradients of the pixels in
       the cell.
    */
    double** hogs;
    int nbCells;
    int nb_cellX;
    int nb_cellY;
};
typedef struct Histograms_s Histograms;


/* Structure storing the HOG descriptors of an image. */
struct HOGDescriptors_s {
    // array containing an object Histograms for each bloc of histograms.
    Histograms* normalizedHOGs;
    int nbBlocs;
    int nb_blocX;
    int nb_blocY;
};
typedef struct HOGDescriptors_s HOGDescriptors;



/*****************************************************************************
 * Compute hogs of all images in the given directory, and write them in a file
 * for the classifier svm svm_light.
 *      - dirPath : path to the directory containing the images
 *      - outputPath : path of the file to write to (data will be added to it
 *                     if it already exists)
 *      - target : target value (1 for positives images, -1 for negative images,
 *                 and 0 for classification)
 *      - writeMode : a to add to files, w to rewrite the file.
 ****************************************************************************/
void computeAndWriteHOGsToFile(const char* dirPath, const char* outputPath,
                                int target, const char* writeMode);


/*******************************************************
 *  Compute and return the HOGDescriptors of an image.
 *     - imgPath : path of the image.                  
 *******************************************************/
HOGDescriptors computeHOGDescriptors(const char* imgPath);


/******************************************************************************
 *  Creates an new image where the value of each pixel is the gradient of the
 * original image.
 *     - imgPath : path of the original image
 *     - gradientImgPath : path of the gradient image to create
 *****************************************************************************/
void createGradientImage(const char* imgPath, const char* gradientImgPath);




/* Allocate memory space for a IMGGradient object.
        - return : the IMGGradient object with properly initialized fields
        - width : width of the matching image
        - height : height of the matching image
*/
IMGGradient createIMGGradient(int width, int height);

/* Allocate memory space for a Histograms object.
        - return : the Histograms object with properly initialized fields
        - width : width of the matching image
        - height : height of the matching image
*/
Histograms createHistograms(int with, int height);

/* Allocate memory space for a HOGDescriptors object.
        - return : the HOGDescriptors object with properly initialized fields
        - width : width of the matching image
        - height : height of the matching image
*/
HOGDescriptors createHOGDescriptors(int with, int height);

/* Free memory allocated for all the fields of the IMGGradient object. */
void freeMemoryIMGGradient(IMGGradient* grad);

/* Free memory allocated for all the fields of the Histograms object. */
void freeMemoryHistograms(Histograms* hogs);

/* Free memory allocated for all the fields of the HOGDescriptors object. */
void freeMemoryHOGDescriptors(HOGDescriptors* descriptors);

/* Compute the gradient for each pixel of an image.
        - grad : (modified by call) is the image gradient
        - img : is the image
*/
void gradientComputation(IMGGradient* grad, PGMData* img);

/* Construct the histograms from the image's gradient.
        - grad : the image gradient
        - hogs : (modified by call) the image's histograms
*/
void histogramsConstruction(IMGGradient grad, Histograms* hogs);

/* Construct the HOGDescriptors from the histograms of the image.
   To do this, it groups the cells together into blocks.
        - return : the HOG descriptors of the image, ready to use with a SVM
          classifier for example
        - hogs : the image's Histograms
*/
void blocsConstruction(Histograms hist, HOGDescriptors* descriptors);

/* Concatenate two strings and return the pointer to the resulting string.
 */
char* concatenate(const char* str1, const char* str2);

#endif

