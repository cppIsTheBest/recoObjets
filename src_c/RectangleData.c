//
// Created by matbl on 09/02/2018.
// Used for the manipulation of a custom structure of RectangleData and Lists
//
#include <dirent.h>
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include "RectangleData.h"

/* Creates the list with the appropriate fileName and NULL next */
List* list_create (const char *val)
{
    List *list = malloc(sizeof(List));
    list->next = NULL;

    char *fileName = malloc(11* sizeof(char));
    int i;

    for (i = 0; i < 10; ++i) {
        fileName[i] = val[i];
    }
    fileName[10] = '\0';

    list->fileName = fileName;

    return list;
}

/* Printing list for debug */
void list_print(List *p)
{
    while(p != NULL) {
        printf("%s", p->fileName);
        p = p -> next;
    }
}

/* Appending an element to the List */
void list_append(List *p,const char *val)
{
    while (p->next != NULL) {
        p = p->next;
    }
    List *plist = NULL;
    plist = list_create(val);
    if (plist != NULL) {
        p->next = plist;
        printf("\n");
    } else {
        perror("Unabled to append fileName");
        exit(EXIT_FAILURE);
    }
}

/* Popping an element off the List */
List* list_remove_first(List *p)
{
    List *first = p;
    p = p->next;
    free(first->fileName);
    free(first);
    return p;
}

/* Indicates the length of the List */
int list_length(List *p)
{
    int n=0;
    while(p)
    {
        n++;
        p = p->next;
    }
    return n;
}

/* Free the memory and empty the List */
void list_clear(List *p)
{
    while(p)
    {
        p = list_remove_first(p);
    }
}

/* Paints a rectangle in the picture on the coordinates passed as parameters */
void paintRectangle(PGMData *data, const int topLeftX, const int topLeftY, const int width, const int length)
{
    const int white = 65535;
    //const int black = 0;

    int i, j;

    /* Let's create three rectangle as such : B/W/B */
    /* Only one for now */
    for (j = topLeftX; j < topLeftX + width + 1; ++j) {
        data->matrix[topLeftY][j] = white;
        if (topLeftY + length <= data->row) {
            data->matrix[topLeftY + length][j] = white;
        }
    }

    for (i = topLeftY; i < topLeftY + length + 1; ++i) {
//        printf("%d \n", i);
//        _flushall();
        data->matrix[i][topLeftX] = white;
        if (topLeftX + width <= data->col) {
            data->matrix[i][topLeftX + width] = white;
        }
    }
}

/* Getting a list of all files in a directory */
List* listDirectory(const char *dirName)
{
    List* fileList = NULL;
    struct dirent *lecture;
    DIR *rep;
    rep = opendir(dirName);
    readdir(rep); readdir(rep);
    while ((lecture = readdir(rep))) {
        // Elements are pushed in their order of appearance i.e. their time stamp
        if (fileList != NULL) {
            list_append(fileList, lecture->d_name);
            //   printf("OK %s", fileList->value);
        } else {
            fileList = list_create(lecture -> d_name);
        }

        //printf("%s\n", lecture->d_name);
    }
    closedir(rep);
    return fileList;
}

/* Getting the number of line in a txt file */
int getNumberOfLine(const char *fileName)
{

    int nbLine = 0;
    int c;
    int c2 = '\0';

    FILE *file = fopen(fileName, "r");
    if (file == NULL) {
        perror("Couldn't open file.");
    }

    while((c=fgetc(file)) != EOF)
    {
        if(c=='\n')
            nbLine++;
        c2 = c;
    }

    /* Ici, c2 est égal au caractère juste avant le EOF. */
    if(c2 != '\n')
        nbLine++; /* Dernière ligne non finie */

    fclose(file);

    return nbLine;

}

/* Drawing the rectangle on all the files from a list */
void paintAllRectangles(const char *inputImagesDirectoryName, const char *outputImagesDirectoryName, List *rectangleInfoList)
{

    int i;

    while (rectangleInfoList != NULL) {

        PGMData *pgmFile = malloc(sizeof(PGMData));
        if (pgmFile == NULL) {
            perror("Couldn't allocate memory");
            exit(EXIT_FAILURE);
        }
        char path[64];
        sprintf(path, "%s/%s.pgm", inputImagesDirectoryName, rectangleInfoList->fileName);
        readPGM(path, pgmFile);
        for (i = 0; i < rectangleInfoList->numberOfRectangles; ++i) {
            paintRectangle(pgmFile,
                           rectangleInfoList->rectangleData->coordX,
                           rectangleInfoList->rectangleData->coordY,
                           rectangleInfoList->rectangleData->width,
                           rectangleInfoList->rectangleData->length);
            rectangleInfoList->rectangleData = rectangleInfoList->rectangleData+1;
            free(rectangleInfoList->rectangleData);
        }
        free(rectangleInfoList->rectangleData);

        sprintf(path, "%s/%s.pgm", outputImagesDirectoryName, rectangleInfoList->fileName);
        writePGM(path, pgmFile);

        rectangleInfoList = rectangleInfoList->next;
    }

}

/* Parsing the file for rectangle data to store */
void parseData(struct RectangleData *rectangleData, const char *fileName, List *currentFile)
{

    int i;
    int maxCharReading = 50;

    FILE *file = fopen(fileName, "r");
    if (file == NULL) {
        perror("Couldn't open file.");
    }
    for (i = 0; i <= currentFile->numberOfRectangles; ++i) {

        int wedc;
        char currentLine[maxCharReading];
        memset( currentLine, 0, maxCharReading*sizeof(char) );
        fgets(currentLine, maxCharReading, file);
        sscanf(currentLine, "%s %d %d %d %d",
               currentFile->fileName,
               &rectangleData->coordX,
               &rectangleData->coordY,
               &rectangleData->width,
               &rectangleData->length);

        rectangleData += 1;
    }

    fclose(file);

}

/* Retrieving rectangle data for one file */
void allocateRectangleData(const char *metaDataDirName, List *currentFile)
{

    char path[64];
    sprintf(path, "%s/%s", metaDataDirName, currentFile->fileName);

    int nbLineInFile = getNumberOfLine(path);
    currentFile->numberOfRectangles = nbLineInFile;
    struct RectangleData *rectangleData = (struct RectangleData *)malloc(sizeof(struct RectangleData) * nbLineInFile);
    if (rectangleData == NULL) {
        perror("Memory allocation failure");
        exit(EXIT_FAILURE);
    }
    currentFile->rectangleData = rectangleData;

    parseData(rectangleData, path, currentFile);

}

/* Retrieving the rectangle data from the metadata file associated with the input file */
void getAllRectangleData(const char *metaDataDirName, List *fileList)
{

    while (fileList != NULL) {
        allocateRectangleData(metaDataDirName, fileList);
        fileList = fileList->next;
    }

}
