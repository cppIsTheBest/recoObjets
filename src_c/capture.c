#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdint.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <linux/videodev2.h>
#include <errno.h>
#include <unistd.h>
#include <sys/time.h>

uint8_t *buffer;

/* Header for PGM files (P5 640 480 65535) */
uint8_t header_buffer[17] = {0x50, 0x35, 0x0a, 0x36, 0x34, 0x30, 0x20, 0x34,
                             0x38, 0x30, 0x0a, 0x36, 0x35, 0x35, 0x33, 0x35, 0x0a};
int header_size = 17;

struct timeval now;

/* Used to store the directory in which to store the files */
char *saved_file_name;
/* Used to store the complete name of the files */
char saved_file[256];


/* Wrapper to loop the use of the ioctl function, to send requests to hardware drivers */
int xioctl(int fd, int request, void *arg) {
  int r;
  do {
    r = ioctl(fd, request, arg);
  } while (r == -1 && errno == EINTR);
  return r;  
}


/* Used to configure the camera with the settings that we want : 640x480 raw grayscale */
int print_caps(int fd) {
  struct v4l2_format format = {0};
  format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  format.fmt.pix.width = 640;
  format.fmt.pix.height = 480;
  format.fmt.pix.pixelformat = V4L2_PIX_FMT_GREY;
  format.fmt.pix.field = V4L2_FIELD_NONE;

  if (xioctl(fd, VIDIOC_S_FMT, &format) == -1) {
    perror("Unable to query webcam");
    return 1;
  }

  return 0;
}


/* Used to init and get the buffer in which will be stored the pictures captured by the camera */
int init_mmap(int fd) {
  struct v4l2_requestbuffers req = {0};
  req.count = 1;
  req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  req.memory = V4L2_MEMORY_MMAP;

  if (xioctl(fd, VIDIOC_REQBUFS, &req) == -1) {
    perror("Unable to request buffer");
    return 1;
  }

  struct v4l2_buffer buf = {0};
  buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  buf.memory = V4L2_MEMORY_MMAP;
  buf.index = 0;
  
  if (xioctl(fd, VIDIOC_QUERYBUF, &buf) == -1) {
    perror("Unable to query buffer");
    return 1;
  }

  buffer = mmap(NULL, buf.length, PROT_READ | PROT_WRITE, MAP_SHARED, fd, buf.m.offset);

  return 0;
}


/* Called periodically to actually capture and save the images */
int capture_image(int fd) {
  struct v4l2_buffer buf = {0};
  buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  buf.memory = V4L2_MEMORY_MMAP;
  buf.index = 0;
  
  if (xioctl(fd, VIDIOC_QBUF, &buf) == -1) {
    perror("Unable to query buffer");
    return 1;
  }

  if (xioctl(fd, VIDIOC_STREAMON, &buf.type) == -1) {
    perror("Unable to start capture");
    return 1;
  }

  fd_set fds;
  FD_ZERO(&fds);
  FD_SET(fd, &fds);
  struct timeval tv = {0};
  tv.tv_sec = 2;

  if (select(fd+1, &fds, NULL, NULL, &tv) == -1) {
    perror("Error while waiting for frame");
    return 1;
  }

  if (xioctl(fd, VIDIOC_DQBUF, &buf) == -1) {
    perror("Error while retrieving frame");
    return 1;
  }

  // Here we save the full filename in saved_file, using the current timestamp
  gettimeofday(&now, NULL);
  sprintf(saved_file, "%s/%ld.pgm", saved_file_name, (now.tv_sec * 1000 + now.tv_usec/1000));

  // Creation of the file 
  int saved_fd = open(saved_file, O_CREAT | O_WRONLY, 0666);
  // Writing the header
  write(saved_fd, header_buffer, header_size);
  // Writing the image data
  write(saved_fd, buffer, buf.length);
  // Closing the file
  close(saved_fd);

  return 0;
}

int main(int argc, char *argv[]) {
  if (argc != 2) {
    printf("Usage : %s DIR\n\nThe images will be saved in DIR (must exist!)\n", argv[0]);
    return 1;
  }

  // Here we get the directory specified on the command line
  saved_file_name = argv[1];

  // File descriptor to the camera
  int web_fd;
  web_fd = open("/dev/video0", O_RDWR);

  if (web_fd == -1) {
    perror("Unable to open webcam at /dev/video0");
    return -1;
  } 

  // Configuration of the camera
  print_caps(web_fd);

  // Initialisation of the buffer for the camera
  init_mmap(web_fd);

  // Periodically capture the images (10Hz)
  while(1) {
    capture_image(web_fd);
    usleep(100000);
  }
 
  return 0;
}
