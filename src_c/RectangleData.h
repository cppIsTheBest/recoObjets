//
// Created by matbl on 09/02/2018.
//

#ifndef PROJETLONG_RECTANGLEDATA_H
#define PROJETLONG_RECTANGLEDATA_H

#include "PGMAPI.h"

/**
 * Structure for rectangles data :
 *     - an int for the top left corner X coordinate
 *     - an int for the top left corner Y coordinate
 *     - an int for the width of the rectangle
 *     - an int for the length of the rectangle
 */
typedef struct RectangleData {
    int coordX;
    int coordY;
    int width;
    int length;
} RectangleData;

/**
 * Structure for listing the data about rectangles in txt files linked to PGM files :
 *     - the name of the file (without extension)
 *     - the data about the rectangles of the file
 *     - the number of rectangles to be drawn into the same file
 *     - the following element of the list
 */
typedef struct List {
    char *fileName;
    struct RectangleData *rectangleData;
    int numberOfRectangles;
    struct List *next;
} List ;

/**
 * Creates a list with just a file name
 * @param val the name of the file
 * @return the created list
 */
List* list_create (const char *val);

/**
 * Prints all the file names of a list
 * @param p the list to be printed
 */
void list_print(List *p);

/**
 * Adds a file name at the end of a list
 * @param p the list that will be modified
 * @param val the new file name to add at the end of the list
 */
void list_append(List *p,const char *val);

/**
 * Removes the first element of a list
 * @param p the list
 * @return the list without its  first element
 */
List* list_remove_first(List *p);

/**
 * Returns the length of a list
 * @param p the list
 * @return the length of the list
 */
int list_length(List *p);

/**
 * Clears the list and free the associated memory space
 * @param p the list to free
 */
void list_clear(List *p);

/**
 * Paints the rectangle into a PGM file with the associated rectangle coordinates
 * @param data the PGM file to be painted
 * @param topLeftX the top left corner X coordinate
 * @param topLeftY the top left corner Y coordinate
 * @param width the width of the rectangle
 * @param length the length of the rectangle
 */
void paintRectangle(PGMData *data, int topLeftX, int topLeftY, int width, int length);

/**
 * Lists all files in a directory and returns a list of those files sorted by default (alphabetical)
 * @param dirName the name of the directory
 * @return the list of all the files contained in the directory
 */
List* listDirectory(const char *dirName);

/**
 * Returns the number of lines in a file
 * @param fileName the name of the file
 * @return the number of line in the file
 */
int getNumberOfLine(const char *fileName);

/**
 * Paints all rectangles from a list and saves the output images in an output directory
 * @param inputImagesDirectoryName the input directory where you get the input PGM files
 * @param outputImagesDirectoryName the output directory where you put the PGM files after painting the rectangles
 * @param rectangleInfoList the list of all data about the rectangles to be painted
 */
void paintAllRectangles(const char *inputImagesDirectoryName, const char *outputImagesDirectoryName, List *rectangleInfoList);

/**
 * Gets the rectangles data from a txt file and stores it in an array of rectangle data
 * @param rectangleData where the data will be stored
 * @param fileName the name of the file to read
 * @param currentFile the list pointing at the current file to be treated
 */
void parseData(struct RectangleData *rectangleData, const char *fileName, List *currentFile);

/**
 * Allocates the appropriate amount of memory for the data to be parsed in the file
 * @param metaDataDirName the name of the directory where the files are
 * @param currentFile the list pointing at the current file being processed
 */
void allocateRectangleData(const char *metaDataDirName, List *currentFile);

/**
 * Gets all the rectangle data from the list
 * @param metaDataDirName the name of the directory where the files are
 * @param fileList the list pointing at the first element to be processes
 */
void getAllRectangleData(const char *metaDataDirName, List *fileList);

#endif //PROJETLONG_RECTANGLEDATA_H
