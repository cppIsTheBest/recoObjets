### Files listing

The files contained in the directory are as follows :

* **capture-aravis** : Program capturing images from the video stream of an ethernet camera into a directory (passed as an argument).

* **capture** : Program capturing images from the video stream of a standard USB camera into a directory (passed as an argument).

* **hog.c** : Source file of the API for HOG (Histogram of Oriented Gradients) computation.

* **hog.c** : Header file of the API for HOG (Histogram of Oriented Gradients) computation.

* **PGMAPI.c** : Source file of the API for reading and writing PGM images.

* **PGMAPI.h** : Header file of the API for reading and writing PGM images.

* **RectangleData.c** : Source file of the API for manipulation of custom detection rectangles.

* **RectangleData.h** : Header file of the API for manipulation of custom detection rectangles.

* **tests/** : Is a sub-directory containing all C files related to the testing of these APIs.
