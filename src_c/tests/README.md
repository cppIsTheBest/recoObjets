### File listing

All the files in this directory define tests for the APIs of the parent folder.

* **CompleteTest.c** : Program testing the good functioning of the RectangleData API.

* **detectionWithSVMLight.c** : Program testing the good functioning of the HOG API.

It computes the HOGs for a sequence of images with a person on it and a sequence of images without a body on it. Then it writes the result to a file that can be given to the SVMLight classifier for training.

Finally, it computes the HOGs for a sequence of images to classify (using the training model) and writes the results to a file that can be given to the SVMLight classifier for classification.

Run the program (after compiling with "make" at the root) without arguments to print usage instructions. **TRAINING\_IMG\_DIR** must be a directory containing two sub-directories called **positives/** and **negatives/**, containing respectively a sequence of images with a person on it and a sequence of images without a person on it. **CLASSIF\_IMG\_DIR** must contain a sequence of images you want to classify using the defined training.

Please note that even if the program works, the training with SVMLight can be extremely slow with full sized images, because the HOGs descriptors are actually computed on the entire image. There is still work to do before being able to use this API in place of the openCV functions.

* **testHOGs.c** : Program testing the good functioning of the HOG API with very simple cases.

