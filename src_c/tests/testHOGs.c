#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include "../hog.h"


void testGradient(char* argv[]);
void testHOGMinImg();


int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("USAGE : %s path_to_test_image_for_gradient\n", argv[0]);
    }
    else {
        /* TEST GRADIENT COMPUTATION */
        printf("\nTEST 1 : gradient computation :\n");
        testGradient(argv);

        /* TEST DESCRIPTORS COMPUTATION for a little image with only one bloc.
           The test isn't exhaustive, it only checks the values for the
           central cell.
        */
        printf("TEST 2 : HOG descriptors computation for a minimal image :\n");
        testHOGMinImg();
        
    }

    return 0;
}



void testGradient(char* argv[]) {
    const char* grad_img_path = "./gradientImgCreatedByHOGTest.pgm";
    createGradientImage(argv[1], grad_img_path);
    printf("    --> Look at the new generated picture %s.\n", grad_img_path);
    printf("        If it represents the gradient of the original img, test");
    printf(" successful.\n\n");
}



void testHOGMinImg() {
    if (BLOC_SIZE % 2 != 1) {
        printf("    -> to run this test, BLOC_SIZE must be odd.\n");
        return;
    }

    // creates minimal image
    const char* minimal_img_path = "./minimalImgForHOGTest.pgm";
    PGMData* img = malloc(sizeof(PGMData));
    img->row = CELL_SIZE * BLOC_SIZE;
    img->col = CELL_SIZE * BLOC_SIZE;
    img->max_gray = 65535;
    img->matrix = allocate_dynamic_matrix(img->row, img->col);
    for (int i=0; i<img->row; i++) {
        for (int j=0; j<img->col; j++) {
            img->matrix[i][j] = i*500;
            if (img->matrix[i][j] > img->max_gray)
                img->matrix[i][j] = img->max_gray;
        }
    }
    writePGM(minimal_img_path, img);

    // compute hog descriptors for the minimal image
    HOGDescriptors descriptors = computeHOGDescriptors(minimal_img_path);

    // check if hog descriptors values for the central cell are correct.
    bool success = true;
    int nx = descriptors.normalizedHOGs[0].nb_cellX;
    int ny = descriptors.normalizedHOGs[0].nb_cellY;
    int h = nx * (ny / 2) + (nx / 2);

    for (int b=0; b<NB_BIN_PER_HISTOGRAM; b++) {
        // In the central cell, all gradient angles should be equal to PI * 2
        if (b == (int)round((NB_BIN_PER_HISTOGRAM-1) / 2)) {
            if (descriptors.normalizedHOGs[0].hogs[h][b] <= 0) {
                success = false;
            }
        }
        else if (descriptors.normalizedHOGs[0].hogs[h][b] != 0) {
            success = false;
        }
    }

    if (success)
        printf("    -> SUCCESS\n");
    else
        printf("    -> FAILURE\n");

    // free memory
    freeMemoryHOGDescriptors(&descriptors);
    deallocate_dynamic_matrix(img->matrix, img->row);
}

