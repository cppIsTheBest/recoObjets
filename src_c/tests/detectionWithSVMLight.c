#include <stdlib.h>
#include "../hog.h"

int main(int argc, char* argv[]) {
    if (argc != 5) {
        printf("USAGE : %s  TRAINING_IMG_DIR  OUPUT_TRAINING_FILE", argv[0]);
        printf("  CLASSIF_IMG_DIR  OUPUT_CLASSIF_FILE\n");
        return 1;
    }

    // get path of sub-directories containing positives and negatives images
    char* positivesPath = concatenate(argv[1], "positives/");
    char* negativesPath = concatenate(argv[1], "negatives/");

    // compute HOGs for positives images
    computeAndWriteHOGsToFile(positivesPath, argv[2], 1, "w");
    // compute HOGs for negatives images
    computeAndWriteHOGsToFile(negativesPath, argv[2], -1, "a");

    // compute HOgs for images that need classification
    computeAndWriteHOGsToFile(argv[3], argv[4], 0, "w");

    // free allocated memory
    free(positivesPath);
    free(negativesPath);

    return 0;
}

