#include <stdio.h>
#include "../RectangleData.h"

//
// Created by matbl on 09/02/2018.
// Executing will paint all the rectangles on all the txt files in the ARGV[3] directory and paint it over
// the PGM files in the ARGV[1] directory into the ARGV[2] directory.
//
int main(int argc, char *argv[])
{
    if (argc < 3) {
        printf("Usage : %s INPUT_DIR OUTPUT_DIR RECTANGLES_DATA_DIR \n"
                       "Where META_DIR : directory in which txt files are located (containing rectangle data) "
                       "\n", argv[0]);
        return 1;
    }

    const char *input_directory = argv[1];
    const char *output_directory = argv[2];
    const char *meta_data_directory = argv[3];

    List *metaDataFileList = NULL;

    metaDataFileList = listDirectory(meta_data_directory);

    getAllRectangleData(meta_data_directory, metaDataFileList);

    paintAllRectangles(input_directory, output_directory, metaDataFileList);

    return 0;
}
