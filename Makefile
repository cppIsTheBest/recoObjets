all: clean .init objects capture detectionWithSVMLight completeTest testHOGs

objects: rectangle_obj pgmapi_obj hog_obj

.init:
	mkdir build

capture:
	gcc src_c/capture.c -o build/capture

detectionWithSVMLight:
	gcc -std=c99 src_c/tests/detectionWithSVMLight.c -o build/detectionWithSVMLight build/hog.o build/PGMAPI.o -lm

completeTest:
	gcc -std=c99 src_c/tests/CompleteTest.c -o build/CompleteTest build/RectangleData.o build/PGMAPI.o 

testHOGs:
	gcc -std=c99 src_c/tests/testHOGs.c -o build/testHOGs build/RectangleData.o build/PGMAPI.o build/hog.o -I src_c/ -lm

hog_obj:
	gcc -std=c99 -c src_c/hog.c -o build/hog.o

rectangle_obj:
	gcc -std=c99 -c src_c/RectangleData.c -o build/RectangleData.o

pgmapi_obj:
	gcc -std=c99 -c src_c/PGMAPI.c -o build/PGMAPI.o

clean:
	if [ -d build ]; then rm -R build; fi

capture-aravis:
	gcc -I ./aravis-0.5.11/src/ -I /usr/include/glib-2.0/ -I /usr/lib/glib-2.0/include/ -I /usr/lib/x86_64-linux-gnu/glib-2.0/include -I /usr/lib/arm-linux-gnueabihf/glib-2.0/include src_c/capture-aravis.c -lglib-2.0 -laravis-0.6 -lgobject-2.0 -o build/capture-aravis
