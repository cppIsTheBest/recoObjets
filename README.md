# Integration of an object recognition function in a mobile robot

### Context

The goal of this project is to implement a person tracking program for an embedded system with a Raspberry Pi using machine learning. The system is composed of a camera placed on top of a mobile robot that is meant to recognize and track someone who is identified via the machine learning process.

Through the process, the program is supposed to detect a person (first its body then its face) in the field of view of the camera, then recognize it from a database. If the person is the one set to be followed, the program will then compute the rotation angle necessary to ensure a good tracking from the robot, which will be sent to the mechanical bits of the system.

It is still a work in progress, but some interesting developements have been done.


### Files listing

* **Makefile** : This file contains instructions to compile every source file in the src\_c directory. To use it, simply type "make" in a terminal.

* **install-dependencies.sh** : This script download and compiles the aravis library, used by the capture-aravis program.

* **src\_c/** : This directory contains all the source files written in C language.

* **src\_python/** : This directory contains all the source files written in python

* **build/** : This directory is generated when the "make" command is used. It contains the compiled executable files.

* **svm\_light/** : This directory contains the sources of the classifier "SVMLight". They can be compiled using the command "make" in a terminal.


### Main features

In this repository you will find several files accomplishing several functionnalities. Not all of them are used in the main functionnal chain because there is sometimes still some work to do.

Here is what has been done :

* A capture program that extracts pictures from a video stream and save them into a directory. It has been tested successfully with the USB webcam Logitech C170 V-U0026. (build/capture)

* An other capture program that extracts pictures from a video stream and save them into a directory. It has been tested successfully with the ethernet camera Gigavision DMK 23GM021. (build/capture-aravis)

* A python program with a GUI that read pictures from a directory in real time, try to detect persons on the pictures, display the detection rectangles on the pictures, and save them into another directory (src\_python/reco\_interface.py).
 
* A C program that compute HOGs on a serie of positive pictures (with a person on it), negative pictures (without a person on it) and test pictures (pictures still to be classified) and write these information into text files. These text files can then be parsed by the classifier SVMLight. (available in the repository)

* A python program that can detect faces on pictures and draw detection rectangles around them.


### Installation

* Before compiling, you should run
```
./install-dependencies.sh
```
at the root of the directory to install the necessary dependencies.

* Then to compile all C programs, simply type
```
make
```
at the root of the directory. It will generate the build directory containing all the executables.

* After that, you can also compile the capture-aravis program if you prefer to use the ethernet camera. For this, execute the following command :
```
make capture-aravis
```
at the root of the directory.

* To use all python programs, you'll need to install opencv, numpy, pyqt5 (but at this point, they should be installed already)


### Utilisation

To execute the prototype, follow these steps :

* Open a terminal at the root of the repository

* Execute the command
```
./build/capture CAPTURE_DIRECTORY
```
or
```
./build/capture-aravis CAPTURE_DIRECTORY
```
Depending on the camera you are using (respectively a regular USB camera or the ethernet camera). CAPTURE\_DIRECTORY is where the pictures will be saved.

* Then open a new terminal at the root of the repository

* And execute the following command
```
python3 src_python/reco_interface.py CAPTURE_DIRECTORY OUTPUT_DIRECTORY
```

With OUTPUT\_DIRECTORY being the directory in which the images with detection rectangles will be saved.

* In the interface that just opened, you can clic on the button "Real time" if you want the interface to process the pictures as they arrive.

