# -*-coding:Utf-8 -*
import numpy as np
import cv2
import pdb
from tkinter import Tk
from tkinter.filedialog import askopenfilenames

# Convert img into grayscale image
def convertToGray(img):
    b,g,r = cv2.split(img)
    if b.all==g.all and g.all==r.all:
        gray = img
    else:
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    return gray

# Save the rectangles corresponding to the faces detected on an image in a txt file.
# The resulting file has the same name as the file of the image
# INTPUTS :
#   filename : name the file containing the image
#   classifier : classifier used to detect the faces
#   scale_factor : the image size is reduced by this scale in order to increase the chance of a matching size with the model for detection
# OUTPUT :
#   The txt file has the same name as the image file
#   Each line contains "x y width length" of one detection rectangle
#   (x,y) : the coordinates of the top-left corner of the rectangle
#   width : the length of horizontal sides (x-axis) of the rectangle
#   height : the length of vertical sides (y-axis) of the rectangle

def save_rectangles(filename,classifier, scale_factor):
    img = cv2.imread(filename)
    gray = convertToGray(img)
    faces = classifier.detectMultiScale(gray, scale_factor, 5)

    # Get the file name without its extension
    name = filename.split('/')[-1]
    result_filename = name.split('.')[0] + '.txt'

    file = open('results/' + result_filename,"w") 

    for (x,y,w,h) in faces:
        #img = cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)   
        file.write(str(x) + ' ' + str(y) + ' ' +  str(w) + ' ' + str(h) + '\n')

    file.close()



#####################################
## ONE EXAMPLE 
#####################################

## Faces detection with Haar cascade classifier
face_cascade = cv2.CascadeClassifier('data/haarcascade_frontalface_default.xml')
save_rectangles('data/family.jpeg',face_cascade,1.15)
