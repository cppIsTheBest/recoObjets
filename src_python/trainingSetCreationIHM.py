#!/usr/bin/python3

from tkinter import *
from PIL import Image, ImageTk
import os


class PosSaver:
    
    def __init__(self):
        # Dictionary associating a position to a picture name
        self.positions = {}


    def record(self, mousePos, file_name):
        self.positions[file_name] = mousePos[0], mousePos[1]
    

    def saveInFiles(self):
        f_pos = open("positions.txt", "w")
        f_names = open("names.txt", "w")
        for k in self.positions:
            f_pos.write(str(self.positions[k][0]) + " " + str(self.positions[k][1]) + '\n')
            f_names.write(k + '\n')
        f_pos.close()
        f_names.close()


    def getPosition(self, file_name):
        if file_name in self.positions:
            return self.positions[file_name]
        else:
            return (-1, -1)




class MainFrame(Frame):

    def __init__(self, main, pics_dir):
        self.main = main

        self.files = []

        # posSaver will manage the recording of informations in two text files
        self.posSaver = PosSaver()

        # Directories in which to pick the images
        self.pics_dir = pics_dir

        # Size of the images in the directory
        self.imgs_size = 1920, 1080

        # Canvas for the input image
        self.canvas_size = 960, 540
        self.canvas = Canvas(main, width=self.canvas_size[0], height=self.canvas_size[1])
        self.canvas.grid(column=1, row=1)

        # List of image files available
        self.update_files()

        # number of images in the directory
        self.nb_imgs = len(self.files)

        # File name Label
        self.file_name_label = Label(main, text="No files")
        self.file_name_label.grid(column=1, row=3, columnspan=2, padx=10)

        # Display first image
        self.img_index = 0
        self.load_image(self.img_index)

        # Previous button
        self.previous_button = Button(main, text="Previous", command=self.previous_image, bg="blue", fg="white")
        self.previous_button.grid(column=0, row=4, columnspan=1, sticky="N", padx=2)

        # Next button
        self.update_button = Button(main, text="Next", command=self.next_image, bg="blue", fg="white")
        self.update_button.grid(column=2, row=4, columnspan=1, sticky="N", padx=10)

        # Quit button
        self.exit_button = Button(main, text="Quit and save", command=self.saveAndQuit, bg="red", fg="white")
        self.exit_button.grid(column=3, row=3, columnspan=1, sticky="N", padx=10)

        # Bind Mouse clic event
        self.canvas.bind('<Button-1>', self.mouseClic)

        # Set-up cross display
        self.crossDisplayed = False
        self.vert_lineID = -1
        self.hor_lineID = -1


    def mouseClic(self, event):
        file_name = self.files[self.img_index]

        mousePosInCanvas = self.canvas.canvasx(event.x), self.canvas.canvasy(event.y)
        mousePosInImg = self.getPosInImg(mousePosInCanvas)
        #print(str(mousePosInImg[0]) + " " + str(mousePosInImg[1]))

        self.posSaver.record(mousePosInImg, file_name)
        self.drawCrossInCanvas(mousePosInCanvas)


    def getPosInImg(self, mousePosInCanvas):
        return mousePosInCanvas[0] * self.imgs_size[0] / self.canvas_size[0],\
               mousePosInCanvas[1] * self.imgs_size[1] / self.canvas_size[1]


    def getPosInCanvas(self, mousePosInImg):
        return mousePosInImg[0] * self.canvas_size[0] / self.imgs_size[0],\
               mousePosInImg[1] * self.canvas_size[1] / self.imgs_size[1]


    def drawCrossInCanvas(self, mousePos):
        file_name = self.files[self.img_index]

        # delete previous cross if needed
        if (self.crossDisplayed):
            self.canvas.delete(self.vert_lineID)
            self.canvas.delete(self.hor_lineID)

        # create new cross
        thickness = 4
        length = 20
        pos = mousePos[0], mousePos[1] - length/2, mousePos[0], mousePos[1] + length/2
        self.vert_lineID = self.canvas.create_line(pos, width=thickness, fill='red')
        pos2 = mousePos[0] - length/2, mousePos[1], mousePos[0] + length/2, mousePos[1]
        self.hor_lineID = self.canvas.create_line(pos2, width=thickness, fill='red')
        self.crossDisplayed = True 


    def next_image(self):
        self.img_index = (self.img_index + 1) % self.nb_imgs
        self.load_image(self.img_index)


    def previous_image(self):
        self.img_index = (self.img_index - 1) % self.nb_imgs
        self.load_image(self.img_index)


    def update_files(self, real_time=False):
        self.files = os.listdir(self.pics_dir)
        self.files.sort()


    def update_file_name(self):
        file_name = self.files[self.img_index]
        first_file_name = self.files[0]
        self.file_name_label.config(text=file_name)
        self.file_name_label.update()


    def load_image(self, index):
        self.update_file_name()
        file_name = self.files[self.img_index]

        self.canvas.delete(all)
        self.crossDisplayed = False

        new_img = Image.open(self.pics_dir + '/' + self.files[index])
        img_resized = new_img.resize(self.canvas_size, Image.ANTIALIAS)
        img_to_display = ImageTk.PhotoImage(img_resized)
        self.canvas.create_image(0, 0, anchor=NW, image=img_to_display)
        self.canvas.image = img_to_display

        # if there is already a recorded position for this image, draw a cross
        posInImg = self.posSaver.getPosition(file_name)
        if (posInImg != (-1, -1)):
            self.drawCrossInCanvas(self.getPosInCanvas(posInImg))

        self.canvas.update()


    def saveAndQuit(self):
        self.posSaver.saveInFiles()
        self.main.quit()




if __name__ == '__main__':
  if len(sys.argv) != 2:
    print("Usage : " + sys.argv[0] + " DIR_PICS")
    exit(1)

  root = Tk()
  MainFrame(root, sys.argv[1])
  root.mainloop()

