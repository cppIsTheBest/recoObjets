#!/usr/bin/python3

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import *
from PyQt5.QtGui import QPixmap
import os, sys, threading
from bodyDetect import *

class MainFrame(QWidget):

  def __init__(self, in_dir, out_dir):
    super().__init__()
    self.setWindowTitle('reco')

    vbox = QVBoxLayout()
    self.setLayout(vbox)

    hbox_images = QHBoxLayout()

    self.label_in = QLabel()
    hbox_images.addWidget(self.label_in)

    self.label_out = QLabel()
    hbox_images.addWidget(self.label_out)

    vbox.addLayout(hbox_images)
    vbox.addStretch(1)

    self.in_dir = in_dir
    self.out_dir = out_dir

    self.token = False

    self.slider = QSlider(Qt.Horizontal)
    self.slider.setMinimum(0)
    self.slider.setFocusPolicy(Qt.StrongFocus)
    self.slider.setTickPosition(QSlider.TicksBothSides)
    self.slider.setSingleStep(1)
    self.slider.valueChanged.connect(self.load_image)
    vbox.addWidget(self.slider)

    vbox.addStretch(1)

    hbox_labels = QHBoxLayout()

    self.file_name_label = QLabel()
    hbox_labels.addWidget(self.file_name_label)

    self.checkbox = QCheckBox('Real time')
    self.checkbox.stateChanged.connect(self.real_time_mode)
    hbox_labels.addWidget(self.checkbox)

    hbox_labels.addStretch(1)

    button_update = QPushButton("Update")
    button_update.released.connect(self.update_files)
    hbox_labels.addWidget(button_update)

    button_exit = QPushButton("Exit")
    button_exit.released.connect(sys.exit)
    hbox_labels.addWidget(button_exit)

    vbox.addLayout(hbox_labels)

    self.update_files()

    self.load_image()
    self.show()


  def update_files(self, real_time=False):
    self.files = os.listdir(self.in_dir)
    self.files.sort()

    self.update_slider(real_time)


  def update_file_name(self):
    file_name = self.files[self.slider.value()]
    first_file_name = self.files[0]
    self.file_name_label.setText(str((int(file_name[:len(file_name)-4])
                                     - int(first_file_name[:len(first_file_name)-4]))/1000) + "s")


  def update_slider(self, real_time):
    val = self.slider.value()
    nb_files = len(self.files)
    self.slider.setMaximum(len(self.files)-1)
    self.slider.setTickInterval(len(self.files)/10)

    if (real_time):
      self.slider.setValue(nb_files-1)
    else:
      self.slider.setValue(val)


  def real_time_mode(self):
    if (self.checkbox.isChecked()):
      self.slider.setEnabled(False)
      self.schedule_update()
    else:
      self.slider.setEnabled(True)


  def schedule_update(self):
    if (self.checkbox.isChecked()):
      threading.Timer(0.1, self.schedule_update).start()
      self.update_files(True)


  def load_image(self):
    # The token is only here to make sure load_image is not called multiple times
    if not self.token:
      self.token = True
    else:
      return

    # display original image
    origin_img_path = self.in_dir + '/' + self.files[self.slider.value()]
    self.pixmap_in = QPixmap(origin_img_path)
    self.label_in.setPixmap(self.pixmap_in)

    # detect person
    img = cv2.imread(origin_img_path)
    nb_loc,loc,w = detectPeople(img)

    # if there is a person detected, creates an image with detection rectangles
    # into the out_dir
    if nb_loc > 0:
        gen_img_path = self.out_dir + '/' + self.files[self.slider.value()]
        for l in loc:
            cv2.rectangle(img, (l[0], l[1]), (l[0]+l[2], l[1]+l[3]), (255, 0, 0), 3)
        cv2.imwrite(gen_img_path, img)
        self.pixmap_out = QPixmap(gen_img_path) 
    # else if nothing is detected, display the original image
    else:
        self.pixmap_out = QPixmap(origin_img_path)

    self.label_out.setPixmap(self.pixmap_out)

#    self.update_file_name()
    self.token = False


if __name__ == '__main__':
  if len(sys.argv) != 3:
    print("Usage : " + sys.argv[0] + " DIR_IN DIR_OUT")
    exit(1)

  app = QApplication(sys.argv)
  ex = MainFrame(sys.argv[1], sys.argv[2])
  sys.exit(app.exec_())
