# -*-coding:Utf-8 -*
import numpy as np
import cv2
import pdb
# python3 -m pdb myscript.py
from tkinter import Tk
from tkinter.filedialog import askopenfilenames


# Detect faces with a specified classifier on an image
# Show the result by drawing a rectangle for each face detected
def detect_faces(img, classifier, scale_factor):
    gray = convertToGray(img)
    faces = classifier.detectMultiScale(gray, scale_factor, 5)
    
    for (x,y,w,h) in faces:
        ## Draw a blue rectangle for each face detected
        img = cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
                
    print('Press any key to close the window')   
    cv2.namedWindow('img', cv2.WINDOW_NORMAL)
    cv2.imshow('img',img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

# Convert img into grayscale image
def convertToGray(img):
    b,g,r = cv2.split(img)
    if b.all==g.all and g.all==r.all:
        gray = img
    else:
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    return gray


######################################
## Faces detection with Haar cascade classifier
######################################
face_cascade = cv2.CascadeClassifier('data/haarcascade_frontalface_default.xml')

#####################################
## Read the image file
#####################################

Tk().withdraw() # we don't want a full GUI, so keep the root window from appearing
filename = askopenfilenames(title="Ouvrir une image") # show an "Open" dialog box and return the path to the selected file

for f in filename:
    img = cv2.imread(f)
    detect_faces(img, face_cascade, 1.15)

