import numpy as np
import cv2
import pdb
# python3 -m pdb myscript.py
from tkinter import Tk
from tkinter.filedialog import askopenfilenames
import math
import random



# Parameters
# ----------
# image : a grayscale image
# noise_typ : a string containing the type of noise the user wants to add to the image
# One of the following strings, selecting the type of noise to add:    
#    'gauss'     Gaussian-distributed additive noise.
#    's&p'       Replaces random pixels with 0 or 1.
#
# Returns a float image with the chosen noise added an the input image.

def noisy(noise_typ,image):
    row,col = image.shape[:2]
    noisy = image.copy()

    if noise_typ == "gauss":
        mean = 10
        sigma = 10
        for j in range(row):
            for i in range(col):
                v = int(math.floor(image[j][i]+random.gauss(0,sigma)))
                if v > 255:
                    v = 255
                if v<0:
                    v = 0
                noisy[j][i] = v

    elif noise_typ == "s&p":
        s_vs_p = 0.5
        amount = 0.006
        # Salt mode
        num_salt = np.ceil(amount * image.size * s_vs_p)
        coords = [np.random.randint(0, i - 1, int(num_salt))
                for i in image.shape]
        noisy[coords] = 255
        
        # Pepper mode
        num_pepper = np.ceil(amount* image.size * (1. - s_vs_p))
        coords = [np.random.randint(0, i - 1, int(num_pepper))
                for i in image.shape]
        noisy[coords] = 0
    return noisy



#####################################################
## This program applies a blur function (gaussian filter), a gaussian noise and a salt-and-pepper noise to the chosen images 
## The results are displayed in windows then saved in the repository "transform_results"
#####################################################


# Ask the user to choose image files
Tk().withdraw() # we don't want a full GUI, so keep the root window from appearing
files = askopenfilenames(title="Ouvrir une image") # show an "Open" dialog box and return the path to the selected file



for f in files:
    img = cv2.imread(f)
    gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # Apply a gaussian filter of size (5,5) to blur the image
    window_size_blur = (5,5)
    blurred_img = cv2.GaussianBlur(img,window_size_blur,0)

    # Apply a gaussian noise
    gauss_img = noisy("gauss", gray_img)
    gauss_img = gauss_img.astype('uint8')

    # Apply a salt-and-pepper noise
    sp_img = noisy("s&p", gray_img)
    sp_img = sp_img.astype('uint8')


    # Convert the resulting image into grayscale image
    blurred_img = cv2.cvtColor(blurred_img, cv2.COLOR_BGR2GRAY)
 
    # Display the initial image and the results
    print('Press any key to close the windows')  
    cv2.namedWindow('Initial image', cv2.WINDOW_NORMAL)
    cv2.imshow('Initial image',img)

    cv2.namedWindow('Blur result', cv2.WINDOW_NORMAL)
    cv2.imshow('Blur result',blurred_img)

    cv2.namedWindow('Gauss result', cv2.WINDOW_NORMAL)
    cv2.imshow('Gauss result',gauss_img)

    cv2.namedWindow('Salt&Pepper result', cv2.WINDOW_NORMAL)
    cv2.imshow('Salt&Pepper result',sp_img)

    cv2.waitKey(0)
    cv2.destroyAllWindows()
    
    # Get the file name without its extension 
    filename = f.split('/')[-1]

    # Save the blur image and the image with noise
    blur_filename = 'transform_results/' +  filename.split('.')[0] + '_blur.pgm'
    cv2.imwrite(blur_filename, blurred_img)

    gauss_filename = 'transform_results/' +  filename.split('.')[0] + '_gauss.pgm'
    cv2.imwrite(gauss_filename, gauss_img)

    sp_filename = 'transform_results/' +  filename.split('.')[0] + '_sp.pgm'
    cv2.imwrite(sp_filename, sp_img)

