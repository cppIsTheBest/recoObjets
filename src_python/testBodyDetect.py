from bodyDetect import detectPeople
from cv2 import ml_SVM as SVM
from cv2 import HOGDescriptor as HOG
import cv2
import sys
import os
import numpy
from termcolor import colored
from time import time


''' Script de test

    Indique :
        - Le taux de faux positif/négatif
        - La proportion de plantage
        - Le nombre total d'images de test
        - Le temps moyen d'un appel à la fonction de détection
'''


#svmPath = sys.argv[2] if len(sys.argv) > 3 else ''
testBasePath = sys.argv[1]



testBasePath = testBasePath if testBasePath[-1] == '/' else testBasePath + '/'

#groundTruth = numpy.loadtxt(testBasePath + 'GroundTruth.txt')
#classifier = SVM.load(svmPath)
#descriptor = DEFAULT['descriptor']


positivesPath = testBasePath + 'positives/'
negativesPath = testBasePath + 'negatives/'

falsePositive = 0
falseNegative = 0
error = 0
computationTimes = []
n = len(os.listdir(positivesPath))
for k,f in enumerate(sorted(os.listdir(positivesPath))):
    if not k%100:
        print(k, "/", n)
    filepath = positivesPath + f
    im = cv2.imread(filepath)
    try:
    #if True:
        start = time()
        found,loc,w = detectPeople(im)
        computationTimes.append(time()-start)
        found = found and (loc,groundTruth[k,:])
        for l in loc:
            cv2.rectangle(im,(l[0],l[1]),(l[0]+l[2],l[1]+l[3]),(255,0,0))
            cv2.imwrite('NoCode/Test_Result/'+f,im)
        if(not found):
            falseNegative += 1

    except:
        error += 1
    

for f in os.listdir(negativesPath):
    filepath = negativesPath + f
    im = cv2.imread(filepath)
    try:
        start = time()
        found,loc,w = detectPeople(im)
        computationTimes.append(time()-start)
        if(found):
            falsePositive += 1
    except:
        error += 1






#print(colored('Succes rate')
print(colored('False postive','blue'),10000*falsePositive//len(os.listdir(negativesPath))/100,'%')
print(colored('False negative','blue'),10000*falseNegative//len(os.listdir(positivesPath))/100,'%')   
print(colored('Program failure rate','blue'),10000*error//(len(os.listdir(positivesPath))+len(os.listdir(negativesPath)))/100,'%')
print(colored('Number of test images','blue'),len(os.listdir(positivesPath))+len(os.listdir(negativesPath)))
print(colored('Average computation time','blue'),100000 * sum(computationTimes)//len(computationTimes)/100,'ms')







