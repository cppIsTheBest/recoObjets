import unittest
import save_rectangles
import fnmatch
import os
import numpy as np
import cv2
import pdb
from tkinter import Tk
from tkinter.filedialog import askopenfilenames


class TestSavingRectangle(unittest.TestCase):

    def setUp(self):
        # Approximative position of the face of einstein in einstein.jpg
        self.center = (80,70)
        self.face_cascade = cv2.CascadeClassifier('data/haarcascade_frontalface_default.xml')
        save_rectangles.save_rectangles('data/einstein.jpg',self.face_cascade,1.15)

    # test if a txt file has been created with the same name as the image file
    def test_same_names(self):
        count_file = 0
        for file in os.listdir('results'):
            if fnmatch.fnmatch(file, 'einstein.txt'):
                count_file += 1

        self.assertEqual(count_file, 1)

    # Test if the center of the face is in the detection rectangle 
    def test_center_in_rectangle(self):
        with open('results/einstein.txt','r') as file:
            data = file.read()
            data_no_space = data.replace('\n','').split(' ')
            x,y,w,h = [int(x) for x in data_no_space]
        
        position_x = self.center[0] > x  and self.center[0] < x+w
        position_y = self.center[1] > y  and self.center[1] < y+h

        self.assertTrue(position_x)
                
            
if __name__ == '__main__':
    unittest.main()
