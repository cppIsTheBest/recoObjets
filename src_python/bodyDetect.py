import cv2
from cv2 import HOGDescriptor
import sys
from termcolor import colored
import numpy
from math import ceil


# Tuple parameters muste be power of two
# NOTE It is unwise to change those parameters
winSize = (64,128)
blockSize = (16,16)
blockStride = (8,8)
cellSize = (8,8)
nbins = 9
derivAperture = 1
winSigma = 4.
histogramNormType = 0
L2HysThreshold = 0.2
gammaCorrection = 0
nlevels = 64
defHog = HOGDescriptor(winSize,blockSize,blockStride,cellSize,nbins,derivAperture,winSigma,
                        histogramNormType,L2HysThreshold,gammaCorrection,nlevels)

vecs = defHog.getDefaultPeopleDetector()


defHog.setSVMDetector(vecs)


def detectPeople(im,descriptor = defHog):

    ''' Attemps to detect people on a picture 
        
        @param im, image on which the detection must be made
        @param descriptor Used descriptor if default descriptor is unsuited
        
        
        @returns nb_found : How many people were detected
        @returns loc : tuple of tuple, each containing (x,y,w,d) being
            x,y : Position of top-left corner
            w,d : Size of the bounding box
        @returns weights : Associated weights (value on descriptors)
        '''

    loc,w = descriptor.detectMultiScale(im)

    return len(loc),loc,w


def detectPeopleUnique(im,descriptor = defHog):

    ''' Attemps to detect people on a picture 
        
        @param im, image on which the detection must be made
        @param descriptor Used descriptor if default descriptor is unsuited
        
        
        @returns found : 1 if a positive detection was made, 0 otherwise
        @returns loc : tuple of tuple, each containing (x,y,w,d) being
            x,y : Position of top-left corner
            w,d : Size of the bounding box
        @returns weights : Associated weights (value on descriptors)
        '''


    nb_found,locations,weights = detectPeople(im,descriptor = descriptor)
    
    if nb_found == 0:
        return nb_found,locations,weights

    cy = im.shape[0]//2
    cx = im.shape[1]//2
    dists_centre = [ abs(loc[0] - cx) + abs(loc[1] - cy) for loc in locations]

    imin = dists_centre.index(min(dists_centre))
    one_loc = locations[imin]
    weight = weights[imin]

    return 1,one_loc,weight






def trackPeople(image,previousImage,box,maxCorners = 32,qualityLevel = 0.05,minDistance = 3,corners=numpy.array([])):

    '''
        Tracks a person from a frame to another

        @param image : The new image on which the tracking must be done
        @previousImage : The previousImage on which tracking has been done
        @maxCorners : Maximum number of feature points (only used if corners value is empty)
        @qualityLevel : Number between 0 and 1, defining the quality of the detected point compared the the point with the biggest detection quality. only used if corners value is empty
        @corners : An array, where each line is the coordinate of a feature point. If the array is empty, points of interest will be detected.

        @returns 

    '''
    # Trim picture to get only feature points in ROI
    y,x,dy,dx = box
    deriv = numpy.array([y,x])
    featImage = image[x:x+dx,y:y+dy,0].astype('float32')
    cv2.imwrite('debug.png',featImage)
    if len(corners) == 0:
        corners = cv2.goodFeaturesToTrack(featImage,maxCorners,qualityLevel,minDistance)
        # Dimension reduction due to a problem with the function
        # Added derivation due to searching feature points only in a box
        corners = numpy.array([ c + deriv for c in corners]).reshape((corners.shape[0],corners.shape[2]))
        corners = corners.astype('float32')


    nextPts = None
    nextPts,status,err = cv2.calcOpticalFlowPyrLK(previousImage,image,corners,nextPts) 
    usefulPoints = [ nextPts[k] for k in range(len(nextPts)) if status[k]]
    previousPoints = [corners[k] for k in range(len(nextPts)) if status[k]]

    
    loc,locBottom = getBBox(usefulPoints)
    loc = max(loc[0],0),max(loc[1],0)
    box = (abs(locBottom[0]-loc[0]),abs(locBottom[1]-loc[1]))



    return loc,box,nextPts



def getBBox(points):

    '''
    Returns the rectangle bouding box of a set of points

    @param points : A set of points

    @returns topLeft,botRight : The top left and bottom right corner of the bouding box.

    '''
    minX = min([p[0] for p in points])
    minY= min([p[1] for p in points])
    maxX = max([p[0] for p in points])
    maxY= max([p[1] for p in points])

    return (minX,minY), (maxX,maxY)












        



